import Vue from 'vue';
import VueNativeSock from 'vue-native-websocket';
import store from './store';

export default Vue.use(
  VueNativeSock,
  'ws://rocket.pelidev.com/ws/game',
  {
    store,
    format: 'json',
  },
);
