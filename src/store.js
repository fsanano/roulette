import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const SET_GAME = 'SET_GAME';

export default new Vuex.Store({
  state: {
    game: {
      event: '',
      isConnected: false,
      data: {},
    },
  },
  mutations: {
    SOCKET_ONOPEN(state) {
      state.game.isConnected = true; // eslint-disable-line
    },
    SOCKET_ONCLOSE(state) {
      state.game.isConnected = false; // eslint-disable-line
    },
    SOCKET_ONERROR(state, event) {
      console.error(state, event);
    },
    SOCKET_ONMESSAGE(state, message) {
      state.game.event = message.Event; // eslint-disable-line
    },
    SET_GAME(state, data) {
      state.game.data = data; // eslint-disable-line
    },
  },
  actions: {
    setGame({ commit }, options) {
      commit(SET_GAME, options);
    },
  },
});
