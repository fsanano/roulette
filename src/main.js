import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import socket from './socket';
import momentum from './momentum';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  socket,
  momentum,
  render: h => h(App),
}).$mount('#app');
